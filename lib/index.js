'use strict';
var Client = require('pg').Client;
var promisify = require('util').promisify;

var defaults = {
	tableName: 'acme_challenge'
};

module.exports.create = function(config) {

	var tableName = config.tableName || defaults.tableName;
	var databaseUrl = config.databaseUrl;
	var client;

	return {
		init: function(opts) {
			console.log('init');
			client = new Client({
				connectionString: databaseUrl
			});

			var dropTableQuery = 'DROP TABLE IF EXISTS "' + tableName + '" CASCADE;';
			var createTableQuery = 'CREATE TABLE IF NOT EXISTS "' + tableName + '" (' +
				'"id"   SERIAL , ' +
				'"domain" VARCHAR(255) NOT NULL, ' +
				'"tokenName" VARCHAR(255), ' +
				'"keyAuthorization" VARCHAR(255), PRIMARY KEY ("id"));';

			return client.connect().then(function() {
				return client.query(dropTableQuery + createTableQuery)
					.then(function(res) {
						// console.log(res);

						return null;
					}).catch(function(err) {
						console.error(err.stack);
						throw err;
					});
			}).catch(function(err) {
				console.error('Unable to connect to the database:', err);
				throw err;
			});

		},

		set: function(data) {
			// console.log('Add Key Auth URL', data);

			var ch = data.challenge;

			var setQuery = 'INSERT INTO "'+tableName+'" ("id","domain","tokenName","keyAuthorization") VALUES (DEFAULT,$1,$2,$3) RETURNING *;';
			var setValues = [ch.identifier.value,ch.token,ch.keyAuthorization];

			return client.query(setQuery,setValues)
				.then(function(res) {
					// console.log(res);
					console.log(
						'acmeChallenge auto-generated ID:',
						res.rows[0].id
					);
					return true;

				}).catch(function(err) {
					console.error(err.stack);
					throw err;
				});

		},
		get: function(data) {
			// console.log('List Key Auth URL', data);

			var ch = data.challenge;

			var getQuery = 'SELECT "id", "domain", "tokenName", "keyAuthorization" FROM "'+tableName+'" AS "acme_challenge" WHERE "acme_challenge"."domain" = $1 AND "acme_challenge"."tokenName" = $2 LIMIT 1;';
			var getValues = [ch.identifier.value,ch.token];

			return client.query(getQuery,getValues)
				.then(function(res) {
					// console.log(res);
					var acmeChallenge = res.rows[0];
					if (acmeChallenge) {
						return {
							keyAuthorization: acmeChallenge.keyAuthorization
						};
					}
					return null;


				}).catch(function(err) {
					console.error(err.stack);
					throw err;
				});
		},

		remove: function(data) {
			var ch = data.challenge;

			var deleteQuery = 'DELETE FROM "'+tableName+'" WHERE "domain" = $1 AND "tokenName" = $2';
			var deleteValues = [ch.identifier.value,ch.token];

			return client.query(deleteQuery,deleteValues)
				.then(function(res) {
					// console.log(res);
					return null;

				}).catch(function(err) {
					console.error(err.stack);
					throw err;
				});

		}
	};
};

// close connection
// client.end();
